#ifndef MISC_H
#define MISC_H

#include <pthread.h>
#include <stdio.h>

extern FILE *logfile;

#define LOGPRINT(x) do { \
    log_lock(); \
    fprintf x; \
    log_unlock(); \
} while (0);

char *strip_leading_path(const char *path, const char *lead);
time_t file_mtime(const char *path);
ssize_t file_size(const char *path);
void log_lock(void);
void log_unlock(void);
int parse_string_to_long_long(const char *in, long long *out);
int parse_string_to_long(const char *in, long *out);

#endif /* MISC_H */
