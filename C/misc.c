#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#define HAVE_LONG_LONG_INT 1
#include <inttypes.h>
#include "xstrtol.h"
#include "misc.h"

static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;

void log_lock(void)
{
    pthread_mutex_lock(&log_mutex);
}

void log_unlock(void)
{
    pthread_mutex_unlock(&log_mutex);
}

char *strip_leading_path(const char *path, const char *lead)
{
    char *ret = (char *)path;
    int len = strlen(lead);

    if (strncmp(path, lead, len) != 0)
        return ret;

    /* else */
    ret += len;

    /* Now strip any leading slashes */
    while (*ret == '/')
        ret++;
    return ret;
}

time_t file_mtime(const char *path)
{
    int res = 0;
    struct stat st;        
    memset(&st, 0, sizeof(st));

    res = lstat(path, &st);
    if (res < 0)
        return -1;

    return st.st_mtime;
}

ssize_t file_size(const char *path)
{
    int res = 0;
    struct stat st;        
    memset(&st, 0, sizeof(st));

    res = lstat(path, &st);
    if (res < 0)
        return -1;

    return st.st_size;
}

int parse_string_to_long_long(const char *in, long long *out)
{
    char *suffix;
    uintmax_t value;
    enum strtol_error e = xstrtoumax(in, &suffix, 10, &value, "bcEGkKMPTwYZ0");
    if (e != LONGINT_OK)
        return EINVAL;
    /* else */
    *out = value;
    return 0;
}

int parse_string_to_long(const char *in, long *out)
{
    errno = 0;
    *out = strtol(in, NULL, 10);
    if (errno)
        return errno;
    /* else */
    return 0;
}
