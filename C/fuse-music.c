#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <pthread.h>
#include <stddef.h>
#include <libgen.h>
#include "fmdb.h"
#include "fmcache.h"
#include "misc.h"

#define FUSE_USE_VERSION 26 /* Latest version */
#include <fuse.h>
#include <fuse_opt.h>

FILE *logfile = NULL;

#define MAIN_DEBUG 0

#if MAIN_DEBUG >= 0
#   define MAINLOG0(x) LOGPRINT(x)
#else
#   define MAINLOG0(x)
#endif

#if MAIN_DEBUG >= 1
#   define MAINLOG1(x) LOGPRINT(x)
#else
#   define MAINLOG1(x)
#endif

#if MAIN_DEBUG >= 2
#   define MAINLOG2(x) LOGPRINT(x)
#else
#   define MAINLOG2(x)
#endif

typedef enum
{
    OUTFMT_OGG = 0,
    OUTFMT_MP3,
    OUTFMT_INVALID
} fmt_e;

typedef struct
{
    char *extension;
    int   extension_len;
    char *default_quality;
    char *encode_command;
} format_t;

format_t formats[] =
{
    { "ogg", 3, "7", "encode_ogg" },
    { "mp3", 3, "4", "encode_mp3" },
};

#define KiB 1024
#define MiB (KiB * KiB)
#define GiB (KiB * KiB * KiB)

#define CACHE_MIN_SIZE (100ULL * MiB)
#define CACHE_DEFAULT_SIZE (10ULL * GiB)
#define NUM_LRU_DIRS 10
#define DIR_TRIGGER 2

static fmt_e format_index = OUTFMT_OGG;
static long long cachesize_value = CACHE_DEFAULT_SIZE;
static long num_encoder_threads_value = 1;
char *quality = NULL;
FMDB *db_state = NULL;

typedef enum
{
    ENCODE_WAITING = 0,
    ENCODE_IN_PROGRESS,
    ENCODE_DONE,
    ENCODE_INVALID
} encode_state_e;

typedef enum
{
    ENCODE_DONT_KEEP = 0,
    ENCODE_KEEP,
} encode_keep_e;

/* Linked list of things to work on / have been worked on */
typedef struct _work_list_t
{
    struct _work_list_t *next;
    int                  error;
    char                 flac_path[PATH_MAX];
    pthread_mutex_t      lock;
    encode_state_e       state;
    pthread_cond_t       cv;
    int                  refcount;
    int                  keep;
} work_list_t;

pthread_t *bg_thread;
struct thread_work
{
    pthread_mutex_t lock;
    int             num_todo;
    pthread_cond_t  cv;
    work_list_t    *work_list;
};
struct thread_work global_work;

#define MOUNT_OPT(n)      char *n
#define FUSE_MOUNT_OPT(n) {"--"#n"=%s", offsetof(struct mount_opts, n), 1}
#define CONFIG_OPT(n)     {#n, offsetof(struct mount_opts, n)}

struct mount_opts
{
    MOUNT_OPT(config_file);    
    MOUNT_OPT(basedir);    
    MOUNT_OPT(cachedir);
    MOUNT_OPT(db_file);
    MOUNT_OPT(format);
    MOUNT_OPT(quality);
    MOUNT_OPT(logfile);
    MOUNT_OPT(cachesize);
    MOUNT_OPT(num_threads);
};
struct mount_opts mo;

static const struct fuse_opt fm_mount_opts[] = {    
    FUSE_MOUNT_OPT(config_file),
    FUSE_MOUNT_OPT(basedir),
    FUSE_MOUNT_OPT(cachedir),
    FUSE_MOUNT_OPT(db_file),
    FUSE_MOUNT_OPT(format),
    FUSE_MOUNT_OPT(quality),
    FUSE_MOUNT_OPT(logfile),
    FUSE_MOUNT_OPT(cachesize),
    FUSE_MOUNT_OPT(num_threads),
    FUSE_OPT_END
};

struct config_opt
{
    char *match;
    unsigned long offset;
};
static const struct config_opt config_opts[] = {
    CONFIG_OPT(config_file),
    CONFIG_OPT(basedir),
    CONFIG_OPT(cachedir),
    CONFIG_OPT(db_file),
    CONFIG_OPT(format),
    CONFIG_OPT(quality),
    CONFIG_OPT(logfile),
    CONFIG_OPT(cachesize),
    CONFIG_OPT(num_threads),
    {NULL, 0}
};

struct
{
    pthread_mutex_t lock;
    char *lru_dirs[NUM_LRU_DIRS];
} dir_state;
pthread_once_t once_control = PTHREAD_ONCE_INIT;
void *bg_handler(void *arg);
void *bg_db_cleanup(void *arg);

static void init_threads(void)
{
    intptr_t i = 0;
    int *threadcount;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK_NP);    

    pthread_mutex_init(&dir_state.lock, &attr);
    pthread_mutex_init(&global_work.lock, &attr);
    pthread_cond_init(&global_work.cv, NULL);
    /* +1 for the DB cleanup thread */
    bg_thread = malloc((1 + num_encoder_threads_value) * sizeof(pthread_t));
    if (!bg_thread)
    {
        MAINLOG0((logfile, "%s: Failed to malloc thread space for thread descriptors\n", __func__));
        db_close(db_state);
    }
    threadcount = malloc((1 + num_encoder_threads_value) * sizeof(int));
    if (!threadcount)
    {
        MAINLOG0((logfile, "%s: Failed to malloc thread space for thread numbers\n", __func__));
        db_close(db_state);
    }
    for (i = 0; i < num_encoder_threads_value; i++)
    {
        threadcount[i] = i;
        (void)pthread_create(&bg_thread[i], NULL, bg_handler, &threadcount[i]);
    }
    (void)pthread_create(&bg_thread[i], NULL, bg_db_cleanup, &threadcount[i]);
    i++;
}

static char *str_encode_state(encode_state_e state)
{
    switch (state)
    {
        case ENCODE_WAITING:
            return "WAITING";
        case ENCODE_IN_PROGRESS:
            return "IN_PROGRESS";
        case ENCODE_DONE:
            return "DONE";
        default:
            return "BOGUS";
    }
}

static void dump_global_queue(void)
{
    work_list_t *entry = global_work.work_list;
    int i = 0;
    int states[ENCODE_INVALID] = {0};

    log_lock();
    fprintf(logfile, "%s:\n", __func__);
    fprintf(logfile, " #  enc_state   keep ref flac\n");
    while (entry)
    {
        fprintf(logfile, "% 3d %11s % 2d   % 2d  %s\n",
                i++, str_encode_state(entry->state),
                entry->keep, entry->refcount, entry->flac_path);
        states[entry->state]++;
        entry = entry->next;
    }
    fprintf(logfile, "found %d entries total:\n", i);
    for (i = 0; i < ENCODE_INVALID; i++)
        fprintf(logfile, "  %s: %d ", str_encode_state(i), states[i]);
    fprintf(logfile, "\n");
    log_unlock();
}

static char *convert_to_base_path(const char *path)
{
    char *basename = strdup(path);
    int length;
    char *out = NULL;
    int ret = 0;

//    fprintf(logfile, "%s: path %s, mo.basedir %s\n", __func__, path, mo.basedir);
    
    if (!basename)
        return NULL;

    length = strlen(basename);
    length -= (formats[format_index].extension_len);
    if ((length >= 1) &&
        (basename[length-1] == '.') &&
        (!strcmp(&basename[length], formats[format_index].extension)))
    {
        basename[length] = 0;
        ret = asprintf(&out, "%s%sflac", mo.basedir, basename);
    }
    else
    {
        ret = asprintf(&out, "%s%s", mo.basedir, basename);
    }

    if (-1 == ret)
    {
        errno = ENOMEM;
        out = NULL;
    }
    
    free(basename);
    return out;
}

static bool is_flac(const char *path)
{
    int length = strlen(path);
    if ( (length >= 5) && /* strlen(".flac") */
         (!strcmp(&path[length-5], ".flac")))
        return true;
    /* else */
    return false;
}

static size_t encoded_size(const char *flac_path, long long *size)
{
    db_size_entry_t in, out;
    int error = 0;
    time_t mtime = file_mtime(flac_path);

    strncpy(in.flac_path, strip_leading_path(flac_path, mo.basedir), sizeof(in.flac_path));
    strncpy(in.format_choice, formats[format_index].extension, sizeof(in.format_choice));
    strncpy(in.format_quality, quality, sizeof(in.format_quality));

    error = db_lookup_size_entry(db_state, &in, &out);
    if (error)
    {
        MAINLOG1((logfile, "%s: returned error %d for %s\n", __func__, error, flac_path));
        return error;
    }
    else if (mtime > out.mtime)
    {
        MAINLOG1((logfile, "%s: file is newer than cached size entry (%ld > %ld) for %s; re-encode\n",
                  __func__, mtime, out.mtime, flac_path));
        return EAGAIN;
    }
    
    *size = out.size;
    return 0;
}

static char *convert_from_flac_name(const char *path)
{
    int length = strlen(path);

    /* length -5(.flac) +1(".") + 1(NULL) */
    char *out = malloc(length - 3 + formats[format_index].extension_len);
    if (!out)
    {
        errno = ENOMEM;
        return NULL;
    }
    memcpy(out, path, length - 5);
    sprintf(&out[length - 5], ".%s", formats[format_index].extension);
    return out;
}

static int call_encoder(int threadnum, const char *flac_path, const char *cache_path)
{
    int error = 0;
    char *command = NULL;

    error = asprintf(&command, "%s -t %d -q %s \"%s\" \"%s\"",
                     formats[format_index].encode_command,
                     threadnum,
                     quality,
                     flac_path,
                     cache_path);
    if (-1 == error)
        return ENOMEM;

    MAINLOG1((logfile, "%s: Ready to run command %s\n", __func__, command));
    error = system(command);
    MAINLOG1((logfile, "%s: system() returned %d\n", __func__, error));
    return error;
}

static int encode_file(int threadnum, const char *flac_path, int keep)
{
    db_size_entry_t size_entry;
    int error = 0;
    long long needed_size = file_size(flac_path);
    time_t flac_mtime = file_mtime(flac_path);
    char *cache_file_name = NULL;
    char cache_path[PATH_MAX];

    /* If we don't have a db entry for the file already, then the only
     * guess that we can make about the encoded size is that it will
     * be smaller than the flac size. Use that as a guide for the
     * space we need. */
    if (0 != encoded_size(flac_path, &needed_size))
    {
        needed_size = file_size(flac_path);
        MAINLOG2((logfile, "%s: No DB entry yet for %s, so we'll have to guess at the flac size (%lld) for cache space\n",
                  __func__, flac_path, needed_size));
    }

    cache_file_name = cache_allocate(mo.basedir, flac_path, needed_size);
    if (!cache_file_name)
    {
        MAINLOG0((logfile, "%s: unable to allocate %lld bytes for a file in the cache, error %d\n",
                  __func__, needed_size, errno));
        return errno;
    }

    sprintf(cache_path, "%s/%s", mo.cachedir, cache_file_name);
    error = call_encoder(threadnum, flac_path, cache_path);
    if (error)
    {
        MAINLOG0((logfile, "%s: encoding failed, error %d\n", __func__, error));
        cache_entry_failed(cache_file_name);
        free(cache_file_name);
        return error;
    }
    
    strncpy(size_entry.flac_path, strip_leading_path(flac_path, mo.basedir), sizeof(size_entry.flac_path));
    strncpy(size_entry.format_choice, formats[format_index].extension, sizeof(size_entry.format_choice));
    strncpy(size_entry.format_quality, quality, sizeof(size_entry.format_quality));
    size_entry.size = file_size(cache_path);
    size_entry.mtime = flac_mtime;

    error = db_store_size_entry(db_state, &size_entry);
    if (error)
    {
        MAINLOG0((logfile, "%s: unable to store size entry for %s, error %d\n",
                  __func__, flac_path, error));
        cache_entry_failed(cache_file_name);
        free(cache_file_name);
        return error;
    }

    error = cache_entry_encode_complete(cache_file_name,
                                        file_size(cache_path),
                                        keep);
    if (error)
    {
        MAINLOG0((logfile, "%s: unable to update cache DB entry for %s, error %d\n",
                  __func__, flac_path, error));
        cache_entry_failed(cache_file_name);
        free(cache_file_name);
        return error;
    }

    free(cache_file_name);
    return 0;
}

static int add_todo_entry_file(const char *flac_path,
                               work_list_t **enqueued,
                               encode_keep_e keep)
{
    work_list_t *entry;
    char dbgname[64];
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK_NP);

    sprintf(dbgname, "%s(%ld):", __func__, (unsigned long)pthread_self());
    
    /* Check if we already have an entry for this file; if so, just
     * add ourselves to the ref count. If not, add a new entry. */
    entry = global_work.work_list;
    while (entry)
    {
        pthread_mutex_lock(&entry->lock);
        if (!strcmp(entry->flac_path, flac_path))
        {
            entry->refcount++;
            *enqueued = entry;
            if (keep)
                entry->keep++;
            pthread_mutex_unlock(&entry->lock);
            MAINLOG1((logfile, "%s: refcount++ on %s\n", dbgname, flac_path));
            return 0;
        }
        pthread_mutex_unlock(&entry->lock);
        entry = entry->next;
    }

    /* If we get here, we didn't find an existing entry */
    entry = calloc(1, sizeof *entry);
    if (!entry)
        return ENOMEM;

    entry->next = NULL;
    entry->error = 0;
    strcpy(entry->flac_path, flac_path);
    pthread_mutex_init(&entry->lock, &attr);
    pthread_cond_init(&entry->cv, NULL);    
    entry->state = ENCODE_WAITING;
    entry->refcount = 1;

    MAINLOG1((logfile, "%s: adding file %s to encode list\n", dbgname, flac_path));

    /* Empty list */
    if (global_work.work_list == NULL)
    {
        global_work.work_list = entry;
    }
    else
    {
        /* Append to list */
        work_list_t *current = NULL;
        current = global_work.work_list;
        while (current->next)
            current = current->next;
        current->next = entry;
    }
    global_work.num_todo++;
    *enqueued = entry;
    return 0;
}

static void free_todo_entry_file(work_list_t *entry)
{
    char dbgname[64];
    sprintf(dbgname, "%s(%ld):", __func__, (unsigned long)pthread_self());

    entry->refcount--;
    if (0 == entry->refcount)
    {
        MAINLOG1((logfile, "%s: entry %s is finished, free it now\n", dbgname, entry->flac_path));
        pthread_cond_destroy(&entry->cv);
        pthread_mutex_unlock(&entry->lock);
        pthread_mutex_destroy(&entry->lock);
        free(entry);
    }
    else
    {
        MAINLOG1((logfile, "%s: entry %s still in use %d time(s)\n", dbgname, entry->flac_path, entry->refcount));
        pthread_mutex_unlock(&entry->lock);
    }        
}

static void *entry_file_thread(void *arg, encode_keep_e keep)
{
    char *flac_path = arg;
    int error = 0;
    work_list_t *entry;
    char dbgname[64];
    sprintf(dbgname, "%s(%ld):", __func__, (unsigned long)pthread_self());
    
    pthread_mutex_lock(&global_work.lock);
    error = add_todo_entry_file(flac_path, &entry, keep);
    free(flac_path);
    if (error)
    {
        pthread_mutex_unlock(&global_work.lock);        
        return NULL;
    }
    MAINLOG1((logfile, "%s: signal, %d files in queue\n", dbgname, global_work.num_todo));
    /* Tell the background thread that we have work to do */
    pthread_cond_broadcast(&global_work.cv);
    pthread_mutex_unlock(&global_work.lock);    

    pthread_mutex_lock(&entry->lock);
    while (entry->state != ENCODE_DONE)
    {
        pthread_cond_wait(&entry->cv, &entry->lock);
    }
    MAINLOG1((logfile, "%s: wakeup, %s done, error %d\n",
              dbgname, entry->flac_path, entry->error));
    error = entry->error;
    free_todo_entry_file(entry);
    return NULL;
}

static void *entry_file_thread_keep(void *arg)
{
    return entry_file_thread(arg, ENCODE_KEEP);
}

static void *entry_file_thread_nokeep(void *arg)
{
    return entry_file_thread(arg, ENCODE_DONT_KEEP);
}

static int add_todo_entry_dir(const char *flac_dir, int keep)
{
    char *flac_path;
    struct dirent *de;        
    int error = 0;
    DIR *dp = opendir(flac_dir);
    long long enc_size = 0;
    if (dp == NULL)
        return errno;
    int should_encode;

    MAINLOG1((logfile, "%s: adding contents of dir %s to encode list\n", __func__, flac_dir));

    /* Start a new thread for each file we want to encode, to deal
     * with cleanup etc. */
    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        should_encode = 1;
        
        if (!asprintf(&flac_path, "%s/%s", flac_dir, de->d_name))
        {
            MAINLOG0((logfile, "%s: asprintf failure\n", __func__));
            return -ENOMEM;
        }
        error = lstat(flac_path, &st);
        if (error < 0)
        {
            MAINLOG0((logfile, "%s: lstat %s returned error %d\n", __func__, flac_path, error));
            free(flac_path);
            closedir(dp);
            return errno;
        }

        if (should_encode && !S_ISREG(st.st_mode))
        {
            /* Not a file - don't encode it */
            should_encode = 0;
            MAINLOG1((logfile, "%s: dirent %s (%s) is not a file, ignore it\n", __func__, de->d_name, flac_path));
        }
        
        if (should_encode && !is_flac(de->d_name))
        {
            /* Not a flac file - don't encode it */
            should_encode = 0;
            MAINLOG1((logfile, "%s: dirent %s (%s) is not a flac, ignore it\n", __func__, de->d_name, flac_path));
        }
        
        if (should_encode && !keep && (0 == encoded_size(flac_path, &enc_size)))
        {
            /* We've been called for readdir(), so we have nokeep. The
             * file already exists in the size cache validly at this
             * point, so there's no need to re-encode it *again* just
             * for a readdir lookahead at this point. */
            should_encode = 0;
            MAINLOG1((logfile, "%s: dirent %s (%s) does not need re-encoding for readdir, ignore it\n",
                      __func__, de->d_name, flac_path));
        }
        
        if (should_encode)
        {
            pthread_t thread;
            pthread_attr_t attr;
            pthread_attr_init(&attr);
            pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
            pthread_attr_setstacksize(&attr, 4 * 1024 * 1024);
            
            if (keep)
                error = pthread_create(&thread, &attr, entry_file_thread_keep, flac_path);
            else
                error = pthread_create(&thread, &attr, entry_file_thread_nokeep, flac_path);
            
            pthread_attr_destroy(&attr);
            if (error)
            {
                MAINLOG0((logfile, "%s: pthread_create returned error %d\n", __func__, error));
                closedir(dp);
                free(flac_path);
                return error;
            }
        }
        else
            free(flac_path);
    }
    closedir(dp);
    return 0;
}

/* Add the current directory to the LRU state at the beginning;
 * shuffle all the other entries down one. Pass back the number of
 * entries that match the new one (including that new one, so will
 * always be 1 or more!). */
static int update_dir_state(char *current_dir, int *num_matches)
{
    int i = 0;
    int matches = 0;
    char dbgname[64];
    sprintf(dbgname, "%s(%ld):", __func__, (unsigned long)pthread_self());

    pthread_mutex_lock(&dir_state.lock);

    /* Free the last one on the current list */
    free (dir_state.lru_dirs[NUM_LRU_DIRS - 1]);

    /* Shuffle the others down, checking for matches as we go */
    for (i = NUM_LRU_DIRS - 1; i >= 1; i--)
    {
        dir_state.lru_dirs[i] = dir_state.lru_dirs[i-1];
        if ((NULL != dir_state.lru_dirs[i]) && 
	    (!strcmp(current_dir, dir_state.lru_dirs[i])))
            matches++;
    }

    /* Copy the new one into place */
    dir_state.lru_dirs[0] = strdup(current_dir);
    if (NULL == dir_state.lru_dirs[0])
    {
        pthread_mutex_unlock(&dir_state.lock);
        return ENOMEM;
    }

    /* Add 1 for the one we just added */
    matches++;    

    MAINLOG1((logfile, "%s: added saved dir %s, now called %d times in recent history (last %d)\n",
              dbgname, current_dir, matches, NUM_LRU_DIRS));
    *num_matches = matches;
    pthread_mutex_unlock(&dir_state.lock);
    return 0;
}    

static int enqueue_encode_file(const char *flac_path, encode_keep_e keep)
{
    int error = 0;
    char *tmp_path = NULL;
    char *current_dir;
    work_list_t *entry;
    char dbgname[64];
    sprintf(dbgname, "%s(%ld):", __func__, (unsigned long)pthread_self());

    pthread_mutex_lock(&global_work.lock);
    error = add_todo_entry_file(flac_path, &entry, keep);
    if (error)
    {
        pthread_mutex_unlock(&global_work.lock);
        return error;
    }

    pthread_mutex_unlock(&global_work.lock);

    if (num_encoder_threads_value > 1)
    {
        int num_matches = 0;

        tmp_path = strdup(flac_path);
        current_dir = dirname(tmp_path);

	error = update_dir_state(current_dir, &num_matches);
        if (error)
        {
            free(tmp_path);
            pthread_mutex_unlock(&global_work.lock);
            MAINLOG0((logfile, "%s: update_dir_state failed, error %d\n",
                      dbgname, error));
            return error;
        }

        if (num_matches >= DIR_TRIGGER)
        {
            error = add_todo_entry_dir(tmp_path, keep);
            if (error)
            {
                free(tmp_path);
                pthread_mutex_unlock(&global_work.lock);
                MAINLOG0((logfile, "%s: add_todo_entry_dir failed, error %d\n",
                          dbgname, error));
                return error;
            }
        }
        free(tmp_path);
    } /* num_encoder_threads_value > 1 */
 
    pthread_mutex_lock(&global_work.lock);
    MAINLOG1((logfile, "%s: signal, %d files in queue\n", dbgname, global_work.num_todo));
    /* Tell the background thread that we have work to do */
    pthread_cond_broadcast(&global_work.cv);
    pthread_mutex_unlock(&global_work.lock);

    pthread_mutex_lock(&entry->lock);
    while (entry->state != ENCODE_DONE)
    {
        pthread_cond_wait(&entry->cv, &entry->lock);
    }
    MAINLOG1((logfile, "%s: wakeup, %s done, error %d\n",
              dbgname, entry->flac_path, entry->error));
    error = entry->error;
    free_todo_entry_file(entry);
    return error;
}

static int fm_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;
    char *flac_path = NULL;
    int error = 0;

    pthread_once(&once_control, init_threads);
    MAINLOG1((logfile, "%s: %s\n", __func__, path));
    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, "/") == 0)
    {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
        return 0;
    }

    flac_path = convert_to_base_path(path);
    if (!flac_path)
        return -errno;

    res = lstat(flac_path, stbuf);
    if (res < 0)
    {
        free(flac_path);
        return -errno;
    }

    if (S_ISDIR(stbuf->st_mode))
    {
        free(flac_path);
        return 0;
    }

    if (S_ISREG(stbuf->st_mode) && is_flac(flac_path))
    {
        long long size;
        error = encoded_size(flac_path, &size);
        if (error)
        {
            /* Need to (re-)encode the file. We're only looking for
             * the size right now, so don't mark the file for
             * keeping. */
            error = enqueue_encode_file(flac_path, ENCODE_DONT_KEEP);
            if (error)
            {
                MAINLOG0((logfile, "%s: encoding %s failed with error %d\n",
                          __func__, flac_path, error));
                free(flac_path);
                return -EIO;
            }

            /* Encoding should have updated the database, so look again */
            error = encoded_size(flac_path, &size);
            if (error)
                return -error;
        }
        stbuf->st_mode = S_IFREG | 0444;
        stbuf->st_nlink = 1;
        stbuf->st_size = size;
        stbuf->st_blocks = (size+511)/512;
    }
    
    free(flac_path);
    return 0;
}

static int fm_readlink(const char *path, char *buf, size_t size)
{
    int res = 0;

    pthread_once(&once_control, init_threads);
    MAINLOG1((logfile, "%s: %s\n", __func__, path));
    res = readlink(path, buf, size - 1);
    if (res == -1)
        return -errno;

    buf[res] = '\0';
    return 0;
}

static int fm_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                      off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    char *flac_path = NULL;

    pthread_once(&once_control, init_threads);

    (void) offset;
    (void) fi;

    MAINLOG1((logfile, "%s: %s\n", __func__, path));

    flac_path = convert_to_base_path(path);
    if (!flac_path)
        return -errno;

    dp = opendir(flac_path);
    if (dp == NULL)
    {
        free(flac_path);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;        
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        
        /* Do we have a file, and does it end in .flac? */
        if (S_ISREG(st.st_mode) && is_flac(de->d_name))
        {
            /* We'll have to convert the name */
            int ret = 0;
            char *out = convert_from_flac_name(de->d_name);
            if (!out)
            {
                free(flac_path);
                return -ENOMEM;
            }
            ret = filler(buf, out, &st, 0);
            free(out);
            if (ret)
                break;
        }
        else
        {
            /* Something else, just pass the name through directly */
            if (filler(buf, de->d_name, &st, 0))
                break;
        }
    }
    closedir(dp);
    return 0;
}

static int fm_open(const char *path, struct fuse_file_info *fi)
{
    int fd;
    char *flac_path = NULL;
    int error = 0;

    pthread_once(&once_control, init_threads);

    MAINLOG1((logfile, "%s: %s\n", __func__, path));

    if (fi->flags & (O_RDWR|O_WRONLY))
        return -EROFS;

    flac_path = convert_to_base_path(path);
    if (!flac_path)
        return -errno;

    /* if flac, do something special, otherwise... */
    if (is_flac(flac_path))
    {
        char *cache_file_name = cache_lookup_for_read(mo.basedir, flac_path);
        char cache_full_path[PATH_MAX];
        if (NULL == cache_file_name)
        {
            /* Need to encode the file. We're looking to open the file
             * straight afterwards, so mark the file for keeping in
             * the cache. */
            error = enqueue_encode_file(flac_path, ENCODE_KEEP);
            if (error)
            {
                MAINLOG0((logfile, "%s: encoding %s failed with error %d\n",
                          __func__, flac_path, error));
                free(flac_path);
                return -EIO;
            }
            cache_file_name = cache_lookup_for_read(mo.basedir, flac_path);
            if (NULL == cache_file_name)
            {
                free(flac_path);
                return -EIO; /* Should never get here! */
            }
        }
        sprintf(cache_full_path, "%s/%s", mo.cachedir, cache_file_name);
        fd = open(cache_full_path, fi->flags);
        if (fd == -1)
        {
            cache_read_finished(mo.basedir, cache_file_name);
            free(cache_file_name);
            free(flac_path);
            return -errno;
        }
        free(cache_file_name);
    }
    else
    {
        fd = open(flac_path, fi->flags);
        if (fd == -1)
        {
            free(flac_path);
            return -errno;
        }
    }
    fi->fh = fd;
    free(flac_path);
    return 0;
}

static int fm_release(const char *path, struct fuse_file_info *fi)
{
    char *flac_path = NULL;

    pthread_once(&once_control, init_threads);
    MAINLOG1((logfile, "%s: %s\n", __func__, path));

    close(fi->fh);

    flac_path = convert_to_base_path(path);
    if (!flac_path)
        return -errno;

    /* if flac, do something special, otherwise... */
    if (is_flac(flac_path))
        (void)cache_read_finished(mo.basedir, flac_path);

    free(flac_path);
    return 0;
}

static int fm_read(const char *path, char *buf, size_t size, off_t offset,
                   struct fuse_file_info *fi)
{
    int res;

    pthread_once(&once_control, init_threads);
    MAINLOG1((logfile, "%s: %s\n", __func__, path));

    (void) path;
    res = pread(fi->fh, buf, size, offset);
    if (res == -1)
    {
        MAINLOG0((logfile, "%s: failed, error %d\n", __func__, errno));
        res = -errno;
    }
    return res;
}

static int fm_statfs(const char *path, struct statvfs *stbuf)
{
    int res;

    pthread_once(&once_control, init_threads);
    MAINLOG1((logfile, "%s: %s\n", __func__, path));

    res = statvfs(mo.basedir, stbuf);
    if (res == -1)
        return -errno;

    stbuf->f_bfree = 0;
    stbuf->f_bavail = 0;
    stbuf->f_ffree = 0;
    stbuf->f_favail = 0;

    return 0;
}

static struct fuse_operations fm_oper = {
    .getattr        = fm_getattr,
    .readlink       = fm_readlink,
    .readdir        = fm_readdir,
    .open           = fm_open,
    .release        = fm_release,
    .read           = fm_read,
    .statfs         = fm_statfs,
};

void *bg_handler(void *arg)
{
    int *tmp = arg;
    int threadnum = *tmp;
    char dbgname[64];
    
    sprintf(dbgname, "%s:%d (%ld)", __func__, threadnum, (unsigned long)pthread_self());
    MAINLOG1((logfile, "%s: startup\n", dbgname));
    
    while(1)
    {
        work_list_t *current = NULL;
        int error = 0;

        /* wait to be given some work to do */
        pthread_mutex_lock(&global_work.lock);
        while (0 == global_work.num_todo) {
            pthread_cond_wait(&global_work.cv, &global_work.lock);
        }
        
        MAINLOG1((logfile, "%s: wakeup, %d in queue\n", dbgname, global_work.num_todo));
        if (MAIN_DEBUG > 0)
            dump_global_queue();
        /* Yay! We've got something. Work on the first item on the
         * list */
        current = global_work.work_list;
        while (current)
        {
            pthread_mutex_lock(&current->lock);
            if (current->state == ENCODE_WAITING)
            {
                global_work.num_todo--;
                current->refcount++;
                current->state = ENCODE_IN_PROGRESS;
                pthread_mutex_unlock(&current->lock);
                break;
            }
            pthread_mutex_unlock(&current->lock);
            current = current->next;
        }
        pthread_mutex_unlock(&global_work.lock);

        if (!current)
        {
            MAINLOG0((logfile, "%s: WTF? didn't find an entry to work on\n", dbgname));
            continue;
        }

        /* Drop the list lock and work directly on the entry we picked
         * up. */
        pthread_mutex_lock(&current->lock);
        MAINLOG1((logfile, "%s: asked to encode %s\n", dbgname, current->flac_path));
        pthread_mutex_unlock(&current->lock);
        
        error = encode_file(threadnum, current->flac_path, current->keep);

        /* Time passes ... */

        /* Finished. Now run through the global_work list again to
         * remove the current entry */
        pthread_mutex_lock(&global_work.lock);
        if (current == global_work.work_list)
        {
            global_work.work_list = current->next;
        }
        else
        {
            work_list_t *remove = global_work.work_list;
            work_list_t *prev = remove;
            while (remove)
            {
                if (remove == current)
                {
                    prev->next = current->next;
                    remove = global_work.work_list;
                    break;
                }
                prev = remove;
                remove = remove->next;
            }
            if (!remove)
                MAINLOG0((logfile, "%s:, failed to find global_work list entry for %s!\n",
                          dbgname, current->flac_path));
        }
        pthread_mutex_unlock(&global_work.lock);

        /* Now report what happened back to any/all who are waiting on
         * this entry */
        pthread_mutex_lock(&current->lock);
        MAINLOG1((logfile, "%s: signal %s is done\n", dbgname, current->flac_path));
        current->error = error;
        current->state = ENCODE_DONE;
        pthread_cond_broadcast(&current->cv);
        free_todo_entry_file(current);
        pthread_mutex_unlock(&current->lock);
    }
}

/* Background thread to periodically clean up old size database entries */
void *bg_db_cleanup(void *arg)
{
    int *tmp = arg;
    int threadnum = *tmp;
    char dbgname[64];
    
    sprintf(dbgname, "%s:%d (%ld)", __func__, threadnum, (unsigned long)pthread_self());
    MAINLOG1((logfile, "%s: startup\n", dbgname));
    
    while(1)
    {
        db_cleanup_sizes(db_state, mo.basedir);
        sleep(3600); /* Wait an hour */
    }
}

/* Deal with:
 *
 * ( )*CONFIG_VAR( )*=( )*value( )*
 * 
 * and ignore 
 *
 * ( )*#( )*
 */
static int parse_config_line(int lineno, char *ptr)
{
    char *endptr;
    int len = strlen(ptr);
    int i = 0;

    endptr = &ptr[len-1];
    /* strip leading whitespace, and dump comments */
    while(1)
    {
        if (isspace(*ptr))
            ptr++;
        else if ('#' == *ptr)
            return 0; /* ignore the rest of the line */
        else
            break;
    }

    /* strip trailing whitespace */
    while(1)
    {
        if (endptr <= ptr)
            return 0; /* blank line */
        if (*endptr && !isspace(*endptr))
            break;
        *endptr-- = 0;
    }

    for (i = 0; config_opts[i].match != NULL; i++)
    {
        char **valueptr = NULL;

        if (strncmp(ptr, config_opts[i].match, strlen(config_opts[i].match)))
            continue;
        ptr += strlen(config_opts[i].match);
        while(isspace(*ptr))
            ptr++;
        if (*ptr != '=')
            return EINVAL;
        ptr++;
        while(isspace(*ptr))
            ptr++;
        valueptr = (void *)&mo + config_opts[i].offset;
        *valueptr = strdup(ptr);
        return 0;
    }
    return EINVAL; /* Didn't match anything */
}

static int parse_config_file(void)
{
    FILE *config = NULL;
    char *ptr = NULL;
    ssize_t size = 0;
    size_t linelen = 0;
    int error = 0;
    int lineno = 1;

    if (!mo.config_file)
        return 0;
    
    config = fopen(mo.config_file, "rb");
    if (NULL == config)
    {
        fprintf(stderr, "Failed to open config file %s, error %d\n",
                mo.config_file, errno);
        exit(errno);
    }
    
    while (1)
    {
        errno = 0;
        size = getline(&ptr, &linelen, config);
        if (-1 == size)
        {
            error = errno;            
            break;
        }
        error = parse_config_line(lineno++, ptr);
        if (error)
            break;
    }
    free (ptr);
    fclose(config);
    return error;
}

int main(int argc, char *argv[])
{
    intptr_t i = 0;
    int error = 0;
    struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
    umask(0);

    logfile = stderr;

    fuse_opt_parse(&args, &mo, fm_mount_opts, NULL);

    error = parse_config_file();
    if (error)
    {
        printf("Error in config file %s: %d (%s)\n",
               mo.config_file, error, strerror(error));
        return error;
    }

    if (mo.logfile)
    {
        FILE *newlog;
        fclose(stderr);
        newlog = fopen(mo.logfile, "wb");
        if (!newlog)
        {
            printf("Can't open specified logfile %s for writing, error %d\n",
                    mo.logfile, errno);
            return errno;
        }
        logfile = newlog;
        setvbuf(logfile, (char *) NULL, _IONBF, 0);
    }

    /* Validate options */
    if (!mo.basedir)
    {
        MAINLOG0((logfile, "Need to specify a basedir\n"));
        return EINVAL;
    }
    MAINLOG1((logfile, "mo.basedir %s\n", mo.basedir));

    if (!mo.cachedir)
    {
        MAINLOG0((logfile, "Need to specify a cachedir\n"));
        return EINVAL;
    }

    /* Convert strings into useful integers */
    if (mo.cachesize)
    {
        error = parse_string_to_long_long(mo.cachesize, &cachesize_value);
        if (error)
        {
            MAINLOG0((logfile, "Unable to parse cachesize value: %s\n", mo.cachesize));
            return EINVAL;
        }
    }
    
    if (mo.num_threads)
    {
        error = parse_string_to_long(mo.num_threads, &num_encoder_threads_value);
        if (error)
        {
            MAINLOG0((logfile, "Unable to parse num_threads value: %s\n", mo.num_threads));
            return EINVAL;
        }
    }

    if (cachesize_value < CACHE_MIN_SIZE)
    {
        MAINLOG0((logfile, "Cache size %lld is too small; minimum allowed is %lld\n",
                  cachesize_value, CACHE_MIN_SIZE));
        return EINVAL;
    }

    if (!mo.db_file)
    {
        MAINLOG0((logfile, "Need to specify a db file\n"));
        return EINVAL;
    }

    if (mo.format)
    {
        for (i = 0; i < OUTFMT_INVALID; i++)
        {
            if (!strcmp(mo.format, formats[i].extension))
            {
                format_index = i;
                break;
            }
            if (i == OUTFMT_INVALID)
            {
                MAINLOG0((logfile, "%s not recognised as a valid output format\n", mo.format));
                return EINVAL;
            }
        }
    }
    
    if (mo.quality)
        quality = mo.quality;
    else
        quality = strdup(formats[format_index].default_quality);

    db_state = db_open(mo.db_file);
    if (!db_state)
    {
        MAINLOG0((logfile, "Failed to open database %s\n", mo.db_file));
        return 1;
    }

    error = cache_init(db_state, mo.cachedir, cachesize_value);
    if (error)
    {
        MAINLOG0((logfile, "Failed to init cache %s, error %d\n", mo.cachedir, error));
        db_close(db_state);
        return 1;
    }

    for (i = 0; i < NUM_LRU_DIRS; i++)
        dir_state.lru_dirs[i] = NULL;

    error = fuse_main(args.argc, args.argv, &fm_oper, NULL);
    if (error)
        MAINLOG0((logfile, "fuse_main failed, error %d\n", error));

    error = cache_shutdown();
    if (error)
    {
        MAINLOG0((logfile, "Failed to shutdown cache %s, error %d\n", mo.cachedir, error));
        db_close(db_state);
    }

    db_close(db_state);
    return error;
}
