#!/usr/bin/perl -w

use strict;
use File::Basename;
use File::Find;
use Audio::FLAC::Header;

my ($incue, $outname, $outcue, $outwav, $basedir, $num_tracks, $i);
my (@cue_tracktitles, @cue_tracklengths);
my %flac_data;
my @wanted_files;
my $last_trackoffset_frame = 0;
my $verbose = 0;
my $total_output_frames = 0;

sub split_msf($) {
    my $frame = shift;
    my ($min,$sec,$bytes);
    $bytes = (2352 * $frame) + 44;
    $sec = int($frame / 75);
    $frame -= ($sec * 75);
    $min = int ($sec / 60);
    $sec -= ($min * 60);

    return ($min, $sec, $frame, $bytes);
}

$incue = shift;
while ($incue =~ "-v") {
    $verbose++;
    $incue = shift;
}

$outname = shift;

if (!defined($incue) || !defined($outname)) {
    print "Missing parameters.\n";
    die "USAGE: $0: <input cue file name> <output file name stem>";
}

$outcue = "$outname.cue";
$basedir = dirname($incue);

open(INCUE, "< $incue") or
    die "Can't open input cue file for reading: $!\n";
open(OUTCUE, "> $outcue") or
    die "Can't open output cue file for writing: $!\n";

# Parse and transform the input cue file, and write out the output cue
# file
while (my $_ = <INCUE>) {
    if ($_ =~ m/^PERFORMER (\S+)/) {
        if ($verbose) {
            print "Artist: $1\n";
        }
        print OUTCUE $_;
        next;
    }
    if ($_ =~ m/^TITLE (\S+)/) {
        if ($verbose) {
            print "Album title: $1\n";
        }
        print OUTCUE $_;
        next;
    }
    if ($_ =~ m/^FILE (\S+) WAVE/) {
        if ($verbose) {
            print "Wav file: \"$outname.wav\"\n";
        }
        print OUTCUE "FILE \"$outname.wav\" WAVE\n";
        next;
    }    
    if ($_ =~ m/  TRACK (\d\d) AUDIO/) {
        $num_tracks = $1;
        print OUTCUE $_;
        next;
    }
    if ($_ =~ m/    TITLE \"(.*)\"$/) {
        $cue_tracktitles[$num_tracks] = $1;
        if ($verbose) {
            print "  Track $num_tracks: $cue_tracktitles[$num_tracks]\n";
        }
        print OUTCUE $_;
        next;
    }
    if ($_ =~ m/    INDEX (\d\d) (\d\d):(\d\d):(\d\d)/) {
        my $frames = $4;
        my $seconds = $3;
        my $minutes = $2;
        my $frame_start = ($minutes*60*75) + ($seconds*75) + $frames;
        if ($num_tracks > 1) {
            $cue_tracklengths[$num_tracks - 1] = ($frame_start - $last_trackoffset_frame);
        }
        $last_trackoffset_frame = $frame_start;  
        print OUTCUE $_;
        next;
    }
}
close(INCUE);
close(OUTCUE);
$cue_tracklengths[$num_tracks] = -1;

sub read_flac_data {
    my ($filename, $title, $numframes);
    
    $filename = $File::Find::name;

    if ($verbose > 1) {
        print "Looking at $filename\n";
    }
    
    if ($filename =~ m/\.flac$/) {
        my $flac = Audio::FLAC::Header->new("$filename");
        my $info = $flac->info();
        my $tags = $flac->tags();    

        $title = $tags->{"TITLE"};
        $numframes = $info->{"TOTALSAMPLES"} / 588;
        
        $flac_data{"$filename"}{"title"} = $title;
        $flac_data{"$filename"}{"numframes"} = $numframes;
        if ($verbose > 1) {
            my ($min, $sec, $frame, $bytes) = split_msf($numframes);
            print "  title: $title\n";
            print "  length: $numframes frames ($min:$sec:$frame, $bytes bytes)\n";
        }
    }
}

# Now generate the list of desired input files. The filenames may not
# match at all (e.g. a double album that has been put together in one
# directory so the track numbers don't match up). Check all the flac
# files in the input dir and pick up the ones that match the track
# name and desired length (where possible). The last track won't have
# a length, as that's not stored in the cue file. We can't do much
# more than that...!
find(\&read_flac_data, "$basedir");

for($i = 1; $i <= $num_tracks; $i++) {
    my $found = 0;
    foreach my $key (keys %flac_data) {
        if ($cue_tracktitles[$i] =~ /^\Q$flac_data{$key}{"title"}\E$/ and
            ($flac_data{$key}{"numframes"} == $cue_tracklengths[$i] or
             $cue_tracklengths[$i] == -1)) {
            $found++;
            if ($verbose > 1) {
                print "Exact match for track $i: \"$cue_tracktitles[$i]\", ($cue_tracklengths[$i] frames) in $key\n";
            }
            push(@wanted_files, $key);
            $total_output_frames += $flac_data{$key}{"numframes"};
        }
    }
    if ($found > 1) {
        print "Track $i: \"$cue_tracktitles[$i]\", ($cue_tracklengths[$i] frames):\n";
        die "  Found too many exact flac matches for the cue entry for track $i (found $found)!\n"
    }
    if ($found == 0) {
        print "Track $i: \"$cue_tracktitles[$i]\", ($cue_tracklengths[$i] frames):\n";
        die "  Didn't find any exact flac matches for the cue entry for track $i, fall back to fuzzy match\n";
            foreach my $key (keys %flac_data) {
                if ($cue_tracktitles[$i] =~ /\Q$flac_data{$key}{"title"}\E/ and
                    ($flac_data{$key}{"numframes"} == $cue_tracklengths[$i] or
                     $cue_tracklengths[$i] == -1)) {
                    $found++;
                    if ($verbose > 1) {
                        print "Fuzzy match for track $i: \"$cue_tracktitles[$i]\", ($cue_tracklengths[$i] frames) in $key\n";
                    }
                    push(@wanted_files, $key);
                    $total_output_frames += $flac_data{$key}{"numframes"};
                }
        }
        if ($found == 0) {
            print "Track $i: \"$cue_tracktitles[$i]\", ($cue_tracklengths[$i] frames):\n";
            die "  Failed to find any fuzzy flac matches for the cue entry for track $\n";
        } elsif ($found > 1) {
            print "Track $i: \"$cue_tracktitles[$i]\", ($cue_tracklengths[$i] frames):\n";
            die "  Found too many fuzzy flac matches for the cue entry for track $i (found $found)!\n"
        }
    }
}

if ($verbose > 1) {
    my ($min, $sec, $frame, $bytes) = split_msf($total_output_frames);
    print "Total output size: $total_output_frames ($min:$sec:$frame) or $bytes bytes\n";
}

push (@wanted_files, "$outname.wav");

if ($verbose > 1) {
    print "About to run /usr/bin/sox @wanted_files\n";
}

if ($verbose) {
    print "Creating output WAV file $outname.wav from " . 
        (scalar(@wanted_files) - 1) . " input flac files\n";
}
exec {'/usr/bin/sox'} 'usr/bin/sox', @wanted_files;

print "Now to write the disc:\n";
print "  wodim dev=/dev/cdrw -v -pad -eject -cuefile=$outname.cue -dao";
