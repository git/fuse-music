#ifndef FMCACHE_H
#define FMCACHE_H

/********************
 *
 *  Higher-level cache functions
 *
 ********************/
long long cache_current_usage(void);
char *cache_allocate(const char *basedir, const char *flac_path, long long size_needed);
int cache_entry_encode_complete(const char *cache_file_name, long long size_used, int keep);
int cache_entry_failed(const char *cache_file_name);

char *cache_lookup_for_read(const char *basedir, const char *flac_path);
int cache_read_finished(const char *basedir, const char *flac_path);

int cache_init(FMDB *db_state, char *dir, long long max_size);
int cache_shutdown(void);

#endif /* FMCACHE_H */
