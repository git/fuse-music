#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <pthread.h>
#include <time.h>
#include "fmdb.h"
#include "fmcache.h"
#include "misc.h"
#include "sha1.h"

static int cache_dir_len = 0;
static FMDB *dbp = NULL;
static char *cache_dir = NULL;
static int randomfd = -1;
static long long cache_max_size = 0;
static pthread_mutex_t cache_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t cache_alloc_lock = PTHREAD_MUTEX_INITIALIZER;

#define RANDOM_FILE "/dev/urandom"
#define SHA1_LENGTH 20

#define CACHE_DEBUG 0

#if CACHE_DEBUG >= 0
#   define CACHELOG0(x) LOGPRINT(x)
#else
#   define CACHELOG0(x)
#endif

#if CACHE_DEBUG >= 1
#   define CACHELOG1(x) LOGPRINT(x)
#else
#   define CACHELOG1(x)
#endif

#if CACHE_DEBUG >= 2
#   define CACHELOG2(x) LOGPRINT(x)
#else
#   define CACHELOG2(x)
#endif

#undef CACHE_LOCK_DEBUG
#ifdef CACHE_LOCK_DEBUG
static inline void _lock(pthread_mutex_t *lock, const char *fn, int line)
{
    char *lockname;
    if (lock == &cache_alloc_lock)
        lockname = "alloc lock";
    else
        lockname = "db lock";
    CACHELOG0((logfile, "taking %s at %s:%d\n", lockname, fn, line));
    pthread_mutex_lock(lock);
    CACHELOG0((logfile, "taken %s at %s:%d\n", lockname, fn, line));
}

static inline void _unlock(pthread_mutex_t *lock, const char *fn, int line)
{
    char *lockname;
    if (lock == &cache_alloc_lock)
        lockname = "alloc lock";
    else
        lockname = "db lock";
    CACHELOG0((logfile, "dropping %s at %s:%d\n", lockname, fn, line));
    pthread_mutex_unlock(lock);
    CACHELOG0((logfile, "dropping %s at %s:%d\n", lockname, fn, line));
}

#define LOCK(lock)   _lock(lock, __func__, __LINE__)
#define UNLOCK(lock) _unlock(lock, __func__, __LINE__)
#else
#define LOCK(lock)   pthread_mutex_lock(lock)
#define UNLOCK(lock) pthread_mutex_unlock(lock)
#endif

static int cache_generate_file_name(const char *in_path, char **out_file)
{
    struct
    {
        unsigned char seed[16];
        char path[PATH_MAX];
    } data;
    int ret;
    SHA1_CONTEXT ctx;
    unsigned char *sha1_result;
    int i = 0;
    char *out;
    
    ret = read(randomfd, data.seed, sizeof(data.seed));
    if (ret != sizeof(data.seed))
        return EIO;

    strcpy(data.path, in_path);
    sha1_init_ctx(&ctx);
    sha1_write(&ctx, &data, sizeof(data));
    sha1_finish_ctx(&ctx);
    out = malloc(2*SHA1_LENGTH + 1);
    if (NULL == out)
        return ENOMEM;
    sha1_result = sha1_read(&ctx);
    for (i = 0; i < SHA1_LENGTH; i++)
        sprintf(&out[i*2], "%2.2X", sha1_result[i]);
    out[2*SHA1_LENGTH] = 0;
    *out_file = out;
    return 0;
}

/* Remove the oldest available cache entry, both in the DB and on disk */
static int cache_remove_lru(void)
{
    int error = 0;
    db_cache_entry_t entry;
    int num_entries = 0;
    char cache_file[PATH_MAX];
    long long size;

    LOCK(&cache_mutex);
    error = db_lookup_cache_entry_by_lru(dbp, &entry, &num_entries);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to find LRU entry, error %d, %d currently in use\n",
                   __func__, error, num_entries));
        UNLOCK(&cache_mutex);
        if ( (ENOENT == error) && (num_entries))
        {
            if (CACHE_DEBUG > 0)
                db_dump_cache(dbp, &size);
            return EBUSY;
        }
        /* else */
        return error;
    }

    CACHELOG1((logfile, "%s: deleting LRU entry %s (%lld bytes)\n",
              __func__, entry.cache_path, entry.size));
    sprintf(cache_file, "%s/%s", cache_dir, entry.cache_path);
    error = unlink(cache_file);
    if (error)
    {
        CACHELOG0((logfile, "%s: unable to delete cache file %s, error %d\n",
                  __func__, cache_file, errno));
        UNLOCK(&cache_mutex);
        return errno;
    }    
    
    error = db_remove_cache_entry(dbp, &entry);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to delete cache DB entry for %s, error %d\n",
                  __func__, cache_file, error));
        UNLOCK(&cache_mutex);
        return error;
    }

    UNLOCK(&cache_mutex);
    return 0;
}

char *cache_allocate(const char *basedir, const char *flac_path, long long size_needed)
{
    int error = 0;
    char *cache_file_name = NULL;
    char cache_full_path[PATH_MAX];
    int fd = -1;
    db_cache_entry_t cache_entry;
    int file_name_available = 0;
    long long flac_size = file_size(flac_path);

    LOCK(&cache_alloc_lock);
    CACHELOG1((logfile, "%s: used %lld, max %lld, want to add %lld\n", __func__, cache_current_usage(), cache_max_size, size_needed));

    while (cache_current_usage() + size_needed > cache_max_size)
    {
        CACHELOG1((logfile, "%s: overfull loop; used %lld, max %lld, want to add %lld\n", __func__, cache_current_usage(), cache_max_size, size_needed));
        error = cache_remove_lru();
        if (error)
        {
            if (EBUSY == error)
            {
                /* We're just busy - other users are hogging the
                 * cache; wait a short while and try again */
                CACHELOG0((logfile, "%s: cache_remove_lru failed EBUSY; wait and retry\n", __func__));
                sleep(1);
                error = 0;
            }
            else
            {                
                CACHELOG0((logfile, "%s: cache_remove_lru failed, error %d\n", __func__, error));
                errno = error;
                UNLOCK(&cache_alloc_lock);
                return NULL;
            }
        }
    }

    UNLOCK(&cache_alloc_lock);
    LOCK(&cache_mutex);
    while (!file_name_available)
    {
        error = cache_generate_file_name(flac_path, &cache_file_name);
        if (error)
        {
            errno = error;
            UNLOCK(&cache_mutex);
            return NULL;
        }

        sprintf(cache_full_path, "%s/%s", cache_dir, cache_file_name);
        fd = open(cache_full_path, O_WRONLY|O_CREAT|O_EXCL, 0600);
        if (-1 == fd)
        {
            CACHELOG0((logfile, "%s: allocated space for cache OK, but error %d opening cache file %s\n",
                       __func__, errno, cache_full_path));
            free(cache_file_name);
        }
        else
            file_name_available = 1;
    }
    CACHELOG1((logfile, "%s: allocated and created cache file OK: %s\n", __func__, cache_full_path));
    close(fd);

    strncpy(cache_entry.flac_path, strip_leading_path(flac_path, basedir), sizeof(cache_entry.flac_path));
    strncpy(cache_entry.cache_path, cache_file_name, sizeof(cache_entry.cache_path));
    cache_entry.state = FMDB_CACHE_ENCODING;
    cache_entry.size = flac_size;
    cache_entry.mtime = time(NULL);

    error = db_store_cache_entry(dbp, &cache_entry);
    if (error)
    {
        free(cache_file_name);
        UNLOCK(&cache_mutex);
        return NULL;
    }
    UNLOCK(&cache_mutex);
    return cache_file_name;
}

int cache_entry_encode_complete(const char *cache_file_name, long long size_used, int keep)
{
    int error = 0;
    db_cache_entry_t in, out;
    long long old_size = 0;
    long long size;

    LOCK(&cache_mutex);    
    strncpy(in.cache_path, cache_file_name, sizeof(in.cache_path));

    CACHELOG1((logfile, "%s: looking up cache entry for %s\n", __func__, cache_file_name));

    /* Find the previous record for this cache file */
    error = db_lookup_cache_entry_by_cache_path(dbp, &in, &out);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to find previous entry to match cache file %s\n",
                   __func__, cache_file_name));
        db_dump_cache(dbp, &size);
        UNLOCK(&cache_mutex);    
        return error;
    }

    CACHELOG1((logfile, "%s: Found entry for %s, dumping\n", __func__, cache_file_name));
    if (CACHE_DEBUG > 0)
        db_dump_cache_entry(&out);

    old_size = out.size;

    /* Now update and write the db record back */
    if (keep)
        /* Keep the file around in the cache, as we're about to read
         * from it! */
        out.state = FMDB_CACHE_READING; 
    else
        /* We're done with it now... */
        out.state = FMDB_CACHE_FREE;
    out.size = size_used;
    out.mtime = time(NULL);

    error = db_store_cache_entry(dbp, &out);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to store replacement entry for cache file %s\n",
                   __func__, cache_file_name));
        UNLOCK(&cache_mutex);    
        return error;
    }

    CACHELOG1((logfile, "%s: updating size used; used to take %lld bytes, now %lld\n",
               __func__, old_size, size_used));

    UNLOCK(&cache_mutex);    
    return 0;
}

int cache_entry_failed(const char *cache_file_name)
{
    int error = 0;
    char cache_path[PATH_MAX];
    db_cache_entry_t in, out;
    long long size;

    LOCK(&cache_mutex);    

    CACHELOG1((logfile, "%s: removing entry for cache file %s\n", __func__, cache_file_name));
    strncpy(in.cache_path, cache_file_name, sizeof(in.cache_path));

    /* Find the previous record for this cache file */
    error = db_lookup_cache_entry_by_cache_path(dbp, &in, &out);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to find previous entry to match cache file %s\n",
                   __func__, cache_file_name));
        db_dump_cache(dbp, &size);
        UNLOCK(&cache_mutex);    
        return error;
    }

    /* Now remove the db record */
    error = db_remove_cache_entry(dbp, &out);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to remove entry for cache file %s\n",
                   __func__, cache_file_name));
        db_dump_cache(dbp, &size);
        UNLOCK(&cache_mutex);    
        return error;
    }
    UNLOCK(&cache_mutex);    

    /* Finally, remove the cache file itself */
    sprintf(cache_path, "%s/%s", cache_dir, cache_file_name);
    error = unlink(cache_path);
    if (error)
    {
        CACHELOG0((logfile, "%s: unable to delete cache file %s, error %d\n",
                   __func__, cache_path, errno));
        return errno;
    }    

    return 0;
}

char *cache_lookup_for_read(const char *basedir, const char *flac_path)
{
    int error = 0;
    db_cache_entry_t in, out;
    long long size;

    strncpy(in.flac_path, strip_leading_path(flac_path, basedir), sizeof(in.flac_path));

    LOCK(&cache_mutex);

    /* Look for any existing cache records for this flac file */
    error = db_lookup_cache_entry_by_flac_path(dbp, &in, &out);
    if (error)
    {
        CACHELOG1((logfile, "%s: no cache entry for flac_path %s\n",
                   __func__, in.flac_path));
        if (CACHE_DEBUG > 0)
            db_dump_cache(dbp, &size);
        UNLOCK(&cache_mutex);
        return NULL;
    }

    /* They're only valid if the file is complete */
    if (out.state == FMDB_CACHE_DELETING ||
        out.state == FMDB_CACHE_ENCODING)
    {
        CACHELOG1((logfile, "%s: entry to match flac_path %s isn't complete; encode and wait\n",
                   __func__, in.flac_path));
        UNLOCK(&cache_mutex);
        return NULL;
    }        

    /* Now update and write the db record back */
    out.state = FMDB_CACHE_READING;
    out.mtime = time(NULL);
    error = db_store_cache_entry(dbp, &out);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to store replacement entry for flac_path %s\n",
                   __func__, in.flac_path));
        db_dump_cache(dbp, &size);
    }
    UNLOCK(&cache_mutex);
    CACHELOG1((logfile, "%s: starting to read from cache file %s\n", __func__, out.cache_path));
    return strdup(out.cache_path);
}

int cache_read_finished(const char *basedir, const char *flac_path)
{
    int error = 0;
    db_cache_entry_t in, out;
    long long size;

    strncpy(in.flac_path, strip_leading_path(flac_path, basedir), sizeof(in.flac_path));

    LOCK(&cache_mutex);

    /* Look for any existing cache records for this flac file */
    error = db_lookup_cache_entry_by_flac_path(dbp, &in, &out);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to find previous entry to match flac_path %s\n",
                   __func__, in.flac_path));
        db_dump_cache(dbp, &size);
        UNLOCK(&cache_mutex);
        return ENOENT;
    }

    /* Now update and write the db record back */
    out.state = FMDB_CACHE_FREE; /* We're done with it now */
    out.mtime = time(NULL);
    error = db_store_cache_entry(dbp, &out);
    if (error)
    {
        CACHELOG0((logfile, "%s: failed to store replacement entry for flac_path %s\n",
                   __func__, in.flac_path));
        db_dump_cache(dbp, &size);
    }
    UNLOCK(&cache_mutex);
    CACHELOG1((logfile, "%s: finished reading from cache file %s\n", __func__, out.cache_path));
    return 0;
}

int cache_shutdown(void)
{
    return 0;
}

long long cache_current_usage(void)
{
    long long cache_space_used = 0;

    db_cache_space_used(dbp, &cache_space_used);
    return cache_space_used;
}

/* Init the cache layer. Must only be run at startup, as this runs
 * before appropriate locks etc. are set up properly. */
int cache_init(FMDB *db_state, char *dir, long long max_size)
{
    int error = 0;
    struct stat sb;
    DIR *dp;
    struct dirent *de;
    int startup_cache_files = 0;
    int removed_cache_entries = 0;

    cache_dir = strdup(dir);
    cache_dir_len = strlen(cache_dir);
    cache_max_size = max_size;
    dbp = db_state;

    error = stat(cache_dir, &sb);
    if (error)
    {
        if (ENOENT == errno)
        {
            error = mkdir(cache_dir, 0700);
            if (error)
            {
                CACHELOG0((logfile, "%s: can't create a new cache_dir %s: error %d (%s)\n",
                           __func__, cache_dir, errno, strerror(errno)));
                return errno;
            }
            CACHELOG1((logfile, "%s: Created new cache_dir %s\n", __func__, cache_dir));
        }
        else
        {
            CACHELOG0((logfile, "%s: can't stat cache_dir %s, error %d (%s)\n",
                       __func__, cache_dir, errno, strerror(errno)));
            return errno;
        }
    }

    randomfd = open(RANDOM_FILE, O_RDONLY);
    if (-1 == randomfd)
    {
        CACHELOG0((logfile, "%s: can't open random file %s, error %d\n", 
                   __func__, RANDOM_FILE, errno));
        return errno;
    }

    /* Sync state of the cache DB, and calculate how much space is
     * currently in use. Clean up old files if needed. */
    error = sync_cache_and_db(dbp, cache_dir);
    if (error)
        return error;

    CACHELOG1((logfile, "%s: Checking existing cache files\n", __func__));
    /* Now we've walked through all the DB entries and deleted any
     * dodgy ones, let's see what's left on disk. If we find a cache
     * file with no associated DB entry, delete it. */
    dp = opendir(cache_dir);
    if (dp == NULL)
    {
        CACHELOG0((logfile, "%s: can't open cache_dir, error %d\n", __func__, errno));
        return errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        char cache_file_name[PATH_MAX];
        db_cache_entry_t in, out;
        sprintf(cache_file_name, "%s/%s", cache_dir, de->d_name);

        error = stat(cache_file_name, &sb);
        if (error)
        {
            CACHELOG1((logfile, "%s: unable to stat cache file %s, error %d. Deleting\n",
                       __func__, cache_file_name, error));
            removed_cache_entries++;
            unlink(cache_file_name);
        }
        if (S_ISREG(sb.st_mode))
        {
            strcpy(in.cache_path, de->d_name);
            error = db_lookup_cache_entry_by_cache_path(dbp, &in, &out);
            if (error)
            {
                CACHELOG1((logfile, "%s: error looking up cache file %s in the DB, error %d. Deleting\n",
                           __func__, cache_file_name, error));
                removed_cache_entries++;
                unlink(cache_file_name);
            }
            /* That's enough in terms of checks; there's an entry in
             * the DB already, and we already checked further up that
             * it's the same size as we expect. But add the size to
             * the current cache usage total. */
            startup_cache_files++;
        }
    }
    closedir(dp);
    CACHELOG1((logfile, "%s: statistics:\n", __func__));
    CACHELOG1((logfile, "  found %d cache files at startup\n", startup_cache_files));
    CACHELOG1((logfile, "  cleaned up %d cache files\n", removed_cache_entries));
    CACHELOG1((logfile, "  now have %d cache files\n", startup_cache_files - removed_cache_entries));
    CACHELOG1((logfile, "  using %lld bytes of space for cache files against max of %lld\n",
               cache_current_usage(), cache_max_size));

    if (cache_current_usage() > cache_max_size)
    {
        CACHELOG1((logfile, "%s: using too much cache space (%lld, configured for %lld). Removing files\n",
                   __func__, cache_current_usage(), cache_max_size));
        while (cache_current_usage() > cache_max_size)
            cache_remove_lru();
        CACHELOG1((logfile, "%s: Now down to a valid size; (%lld, configured for %lld). Removing files\n",
                   __func__, cache_current_usage(), cache_max_size));
    }

    return 0;
}
