#ifndef FMDB_H
#define FMDB_H

#include <time.h>
#include <limits.h>

#define FMDB void

/* Cached file details */
typedef struct
{
    char               flac_path[PATH_MAX];     /* The path to the flac file, relative to basedir */
    char               format_choice[10];       /* The choice of output format, in text */
    char               format_quality[10];      /* The choice of output quality, in text */
    long long          size;                    /* The encoded size, in bytes */
    time_t             mtime;                   /* The mtime of the flac file when we did the encoding. */
} db_size_entry_t;

typedef enum
{
    FMDB_CACHE_FREE,        /* Item is not currently in use */
    FMDB_CACHE_ENCODING,    /* We're currently writing to it */
    FMDB_CACHE_READING,     /* We're currently reading from it */
    FMDB_CACHE_DELETING     /* Is currently being deleted */
} db_cache_state_e;

typedef struct
{
    char               flac_path[PATH_MAX];     /* The path to the flac file, relative to basedir */
    char               cache_path[PATH_MAX];    /* The path to the cached output file, relative to cachedir */
    db_cache_state_e   state;                   /* What state is this cache file in? */
    long long          size;                    /* The encoded size, in bytes */
    time_t             mtime;                   /* Last time we touched the cache file, so we can evict by LRU */
} db_cache_entry_t;

/* Cached file details */
typedef struct
{
    int                count;                   /* Result */
} db_count_entry_t;

/* Open / create the database */
FMDB *db_open(char *db_name);

/* Close the database */
int db_close(FMDB *dbp);

/********************
 *
 *  SIZE DB functions
 *
 ********************/
/* Add/replace */
int db_store_size_entry(FMDB *dbp, const db_size_entry_t *in);
/* Lookup */
int db_lookup_size_entry(FMDB *dbp, const db_size_entry_t *in, db_size_entry_t *out);
/* Remove. Needed? */
int db_remove_size_entry(FMDB *dbp, const db_size_entry_t *in);
/* Walk the size database, removing any entries for flac files that no longer exist */
int db_cleanup_sizes(FMDB *dbp, const char *basedir);
/* Print out the members of a size DB entry */
void db_dump_size_entry(const db_size_entry_t *entry);
/* Dump the contents of the DB to the logfile */
int db_dump_size(FMDB *dbp);

/********************
 *
 *  CACHE DB functions
 *
 ********************/
/* Add/replace */
int db_store_cache_entry(FMDB *dbp, const db_cache_entry_t *in);
/* Lookup by flac path */
int db_lookup_cache_entry_by_flac_path(FMDB *dbp, const db_cache_entry_t *in, db_cache_entry_t *out);
/* Lookup by cache path */
int db_lookup_cache_entry_by_cache_path(FMDB *dbp, const db_cache_entry_t *in, db_cache_entry_t *out);
/* Lookup least recently used entry, so we can delete it. Also say how many entries are in use currrently. */
int db_lookup_cache_entry_by_lru(FMDB *dbp, db_cache_entry_t *out, int *num);
/* Remove */
int db_remove_cache_entry(FMDB *dbp, const db_cache_entry_t *in);
/* Print out the members of a cache entry */
void db_dump_cache_entry(const db_cache_entry_t *entry);
/* Dump the whole contents of the DB to the logfile; also pass back the total space currently allocated */
int db_dump_cache(FMDB *dbp, long long *size);
/* Ask how much space is currently allocated */
int db_cache_space_used(FMDB *dbp, long long *size);

/* Synchronise at startup */
int sync_cache_and_db(FMDB *dbp, char *cache_dir);

#endif /* FMDB_H */
